Our focus is on keeping a clean, safe, and friendly facility. Our machines are well maintained and kept clean inside and out and in good working order. Our bathrooms are kept clean and fresh. Our attendants are customer service focused and can help you with whatever you need.

Address: 5038 E Princess Anne Rd, Norfolk, VA 23502, USA

Phone: 757-632-4132

Website: [http://washnspinlaundromat.com](http://washnspinlaundromat.com)
